﻿using System;

namespace ArreglosBidimencionales
{
    public class Program
    {
        // CONSTANTES
        public const int FILAS = 3;
        public const int COLUMNAS = 4;

        // ATRIBUTOS
        private int[,] enteros;

        // METODO CONSTRUCTOR
        public Program()
        {
            enteros = new int[FILAS, COLUMNAS];
            enteros[0, 0] = 1;
            enteros[0, 1] = 3;
            enteros[0, 2] = 5;
            enteros[0, 3] = 7;
            enteros[1, 0] = 5;
            enteros[1, 1] = 4;
            enteros[1, 2] = 1;
            enteros[1, 3] = 16;
            enteros[2, 0] = 7;
            enteros[2, 1] = 9;
            enteros[2, 2] = 61;
            enteros[2, 3] = 13;
        }

        // METODOS
        public void listar()
        {
            for (int i = 0; i < FILAS; i++)
            {
                for (int j = 0; j < COLUMNAS; j++)
                {
                    Console.Write(enteros[i, j] + "  ");
                }
                Console.WriteLine();
            }
        }

        public double promedio()
        {
            double promedio = 0.0;
            double suma = 0.0;
            for (int i = 0; i < FILAS; i++)
            {
                for (int j = 0; j < COLUMNAS; j++)
                {
                    suma = suma + enteros[i, j];
                }
                promedio = suma / (FILAS * COLUMNAS);
            }
            return promedio;
        }

        static void Main(string[] args)
        {
            Program myProgram = new Program();
            myProgram.listar();
            Console.WriteLine("\nEl promedio es igual a " + myProgram.promedio());
        }
    }
}

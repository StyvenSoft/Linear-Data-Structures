﻿using System;

namespace ArregloConDatosPrimitivos
{
    public class Program
    {
		// ATRIBUTOS
		private double[] notas;

		// METODO CONSTRUCTOR
		public Program()
		{
			notas = new double[10];
			notas[0] = 5.0;
			notas[1] = 3.0;
			notas[2] = 3.5;
			notas[3] = 4.0;
			notas[4] = 4.6;
			notas[5] = 2.0;
			notas[6] = 1.0;
			notas[7] = 4.5;
			notas[8] = 3.8;
			notas[9] = 2.6;
		}

		public double promedio1()
		{
			double promedio = 0.0;
			double suma = 0.0;
			for (int i = 0; i < notas.Length; i++)
			{
				suma = suma + notas[i];
				Console.WriteLine("Nota en la posicion " + i + " = " + notas[i]);
			}
			promedio = suma / notas.Length;
			return promedio;
		}

		public double promedio2()
		{
			double promedio = 0.0;
			double suma = 0.0;
			foreach (double nota in notas)
			{
				suma = suma + nota;
				Console.WriteLine("Nota: " + nota);
			}
			promedio = suma / notas.Length;
			return promedio;
		}

		public static void Main(String[] args)
		{
			Program miNotaFinal = new Program();
			Console.WriteLine("El promedio es: " + miNotaFinal.promedio1());
			Console.WriteLine("El promedio es: " + miNotaFinal.promedio2());
		}
	}
}

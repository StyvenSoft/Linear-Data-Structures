﻿using ArreglosConObjetos.mundo;
using System;

namespace ArregloConObjetos
{
	public class Program
	{
		// ATRIBUTOS
		private Estudiante[] estudiantes;

		// METODO CONSTRUCTOR
		public Program()
		{
			estudiantes = new Estudiante[5];
			estudiantes[0] = new Estudiante("111", "Celso", 3, "POO 2");
			estudiantes[1] = new Estudiante("222", "Javier", 5, "Estructuras de Datos");
			estudiantes[2] = new Estudiante("333", "William", 7, "Computación gráfica");
			estudiantes[3] = new Estudiante("444", "Carlos", 2, "POO 1");
			estudiantes[4] = new Estudiante("555", "Ivan", 1, "Introducción a la Ingenieria");
		}

		public Estudiante buscarEstudiante(String codigo)
		{
			Estudiante buscado = null;
			bool centinela = false;
			for (int i = 0; i < estudiantes.Length && centinela == false; i++)
			{
				if (estudiantes[i].codigo.Equals(codigo))
                {
					buscado = estudiantes[i];
					centinela = true;
				}
			}
			return buscado;
		}

		public static void Main(String[] args)
		{
			Program miPrograma = new Program();
			Console.WriteLine("Digite el codigo del estudiante a buscar: ");
			String codigo = Console.ReadLine();
			Console.WriteLine("El estudiante buscado es: " + miPrograma.buscarEstudiante(codigo));
		
		}
	}
}

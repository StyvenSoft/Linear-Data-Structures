﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fase3PilaConsola.mundo
{
    public class Factura
    {
		// ATRIBUTOS
		private String numFactura;
		private String numMatricula;
		private String mesfacturado;
		private String nombre;
		private String direccion;
		private int estrato;
		private String categoria;
		private double mts3Consumidos;

		// METODO CONTRUCTOR
		public Factura(String numFactura, String numMatricula, String mesfacturado,
				String nombre, String direccion, int estrato, String categoria,
				double mts3Consumidos)
		{
			this.numFactura = numFactura;
			this.numMatricula = numMatricula;
			this.mesfacturado = mesfacturado;
			this.nombre = nombre;
			this.direccion = direccion;
			this.estrato = estrato;
			this.categoria = categoria;
			this.mts3Consumidos = mts3Consumidos;
		}

		// METODOS GET Y SET
		public String getNumFactura()
		{
			return numFactura;
		}
		public void setNumFactura(String numFactura)
		{
			this.numFactura = numFactura;
		}
		public String getNumMatricula()
		{
			return numMatricula;
		}
		public void setNumMatricula(String numMatricula)
		{
			this.numMatricula = numMatricula;
		}
		public String getMesfacturado()
		{
			return mesfacturado;
		}
		public void setMesfacturado(String mesfacturado)
		{
			this.mesfacturado = mesfacturado;
		}
		public String getNombre()
		{
			return nombre;
		}
		public void setNombre(String nombre)
		{
			this.nombre = nombre;
		}
		public String getDireccion()
		{
			return direccion;
		}
		public void setDireccion(String direccion)
		{
			this.direccion = direccion;
		}
		public int getEstrato()
		{
			return estrato;
		}
		public void setEstrato(int estrato)
		{
			this.estrato = estrato;
		}
		public String getCategoria()
		{
			return categoria;
		}
		public void setCategoria(String categoria)
		{
			this.categoria = categoria;
		}
		public double getMts3Consumidos()
		{
			return mts3Consumidos;
		}
		public void setMts3Consumidos(int mts3Consumidos)
		{
			this.mts3Consumidos = mts3Consumidos;
		}

		// METODO TOSTRING
		override
		public String ToString()
		{
			return "Factura [numFactura=" + numFactura + ", numMatricula=" + numMatricula + ", mesfacturado=" + mesfacturado
					+ ", nombre=" + nombre + ", direccion=" + direccion + ", estrato=" + estrato + ", categoria="
					+ categoria + ", mts3Consumidos=" + mts3Consumidos + "]";
		}
	}
}

﻿using Fase3PilaConsola.mundo;
using System;

namespace Fase3PilaConsola
{
    class Program
    {
        static void Main(string[] args)
        {
			Empresa acueducto = new Empresa();
			acueducto.agregarFactura("111", "aaa111", "enero",
					 "Celso", "Dir 1", 1, "Invacion", 1.5);
			acueducto.agregarFactura("222", "bbb222", "febrero",
					 "Javier", "Dir 2", 4, "Urbano", 3);
			acueducto.agregarFactura("333", "ccc333", "marzo",
					 "Gafis", "Dir 3", 3, "Residencial", 1.5);
			acueducto.listar();
			Console.WriteLine();
			Console.WriteLine("Ultima en entrar...");
			Console.WriteLine(acueducto.obtenerUltimaEnEntrar());
			Console.WriteLine();
			acueducto.eliminarFactura();
			acueducto.listar();
			Console.WriteLine();
			acueducto.eliminarFactura();
			acueducto.listar();
		}
    }
}

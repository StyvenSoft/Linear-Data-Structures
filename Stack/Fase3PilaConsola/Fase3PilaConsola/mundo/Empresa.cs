﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fase3PilaConsola.mundo
{
    public class Empresa
    {
		private Stack<Factura> pilaAcueducto;

		public Empresa()
		{
			pilaAcueducto = new Stack<Factura>();
		}

		public void agregarFactura(String numFactura, String numMatricula, String mesfacturado,
				String nombre, String direccion, int estrato, String categoria,
				double mts3Consumidos)
		{
			pilaAcueducto.Push(new Factura(numFactura, numMatricula, mesfacturado,
					 nombre, direccion, estrato, categoria, mts3Consumidos));
			Console.WriteLine("Agregada factura No." + " " + numFactura);
		}

		public void eliminarFactura()
		{
			pilaAcueducto.Pop();
			Console.WriteLine("Se elimino el ultimo.");
		}

		public Factura obtenerUltimaEnEntrar()
		{
			Factura ultimaEnEntrar = pilaAcueducto.Peek();
			return ultimaEnEntrar;
		}

		public void listar()
		{
			Factura[] facturas = pilaAcueducto.ToArray();
			for (int i = 0; i < facturas.Length; i++)
			{
				Console.WriteLine(facturas[i] + " posicion: " + i);
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fase3ListaConsola.mundo
{
    class Estudiante
    {
		// ATRIBUTOS
		private String id;
		private String nombre;
		private int edad;
		private int estrato;
		private String programa;
		private String universidad;

		// METODO CONSTRUCTOR
		public Estudiante(String id, String nombre, int edad, int estrato,
				String programa, String universidad)
		{
			this.id = id;
			this.nombre = nombre;
			this.edad = edad;
			this.estrato = estrato;
			this.programa = programa;
			this.universidad = universidad;
		}

		// METODOS GET Y SET
		public String getId()
		{
			return id;
		}
		public void setId(String id)
		{
			this.id = id;
		}
		public String getNombre()
		{
			return nombre;
		}
		public void setNombre(String nombre)
		{
			this.nombre = nombre;
		}
		public int getEdad()
		{
			return edad;
		}
		public void setEdad(int edad)
		{
			this.edad = edad;
		}
		public int getEstrato()
		{
			return estrato;
		}
		public void setEstrato(int estrato)
		{
			this.estrato = estrato;
		}
		public String getPrograma()
		{
			return programa;
		}
		public void setPrograma(String programa)
		{
			this.programa = programa;
		}
		public String getUniversidad()
		{
			return universidad;
		}
		public void setUniversidad(String universidad)
		{
			this.universidad = universidad;
		}

		// METODO TOSTRING
		override
		public String ToString()
		{
			return "Estudiante [id=" + id + ", nombre=" + nombre + ", edad=" + edad + ", estrato=" + estrato + ", programa="
					+ programa + ", universidad=" + universidad + "]";
		}
	}
}

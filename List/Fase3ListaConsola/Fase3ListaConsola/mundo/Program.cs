﻿using Fase3ListaConsola.mundo;
using System;

namespace Fase3ListaConsola
{
    class Program
    {
        static void Main(string[] args)
        {
			Entidad mi = new Entidad();
			mi.agregarAlFinal("111", "Celso", 20, 2, "Ing. Sistemas", "UT");
			mi.agregarAlFinal("222", "Javier", 30, 3, "Ing. Mecanica", "UNAD");
			mi.agregarAlFinal("333", "Andres", 40, 4, "Ing. Civil", "UNAL");
			mi.agregarAlInicio("444", "Pedro", 25, 1, "Ing. Sistemas", "UT");
			mi.agregarAlInicio("555", "Juan", 36, 5, "Ing. Mecanica", "UNAD");
			mi.agregarAlInicio("666", "Rodolfo", 18, 6, "Ing. Civil", "UNAL");
			mi.agregarEnLaPosicion(7, "101010", "Fernando", 23, 5, "Ing. Industrial", "UNAL");
			mi.listar();
			Console.WriteLine("\nEliminar el primero: ");
			mi.eliminarElPrimero();
			mi.listar();
			Console.WriteLine("\nEliminar el ultimo: ");
			mi.eliminarElUltimo();
			mi.listar();
			Console.WriteLine("\nEliminar en posicion: ");
			mi.eliminarElDeLaPosicion(4);
			mi.listar();
		}
    }
}

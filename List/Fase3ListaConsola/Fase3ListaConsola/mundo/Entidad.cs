﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fase3ListaConsola.mundo
{
    class Entidad
    {
		// ATRIBUTOS
		private List<Estudiante> ListaEstudiantes;

		// METODO CONSTRUCTOR
		public Entidad()
		{
			ListaEstudiantes = new List<Estudiante>();
		}

		// METODO AGREGAR AL FINAL
		public void agregarAlFinal(String id, String nombre, int edad, int estrato,
				String programa, String universidad)
		{
			Estudiante nuevo = new Estudiante(id, nombre, edad, estrato,
					programa, universidad);
			ListaEstudiantes.Add(nuevo);
		}

		// METODO AGREGAR AL INICIO
		public void agregarAlInicio(String id, String nombre, int edad, int estrato,
				String programa, String universidad)
		{
			Estudiante nuevo = new Estudiante(id, nombre, edad, estrato,
					programa, universidad);
			ListaEstudiantes.Insert(0, nuevo);
		}

		// METODO AGREGAR EN LA POSICION DADA
		public void agregarEnLaPosicion(int pos, String id, String nombre, int edad, int estrato,
				String programa, String universidad)
		{
			if (pos <= ListaEstudiantes.Count)
			{
				Estudiante nuevo = new Estudiante(id, nombre, edad, estrato,
						programa, universidad);
				ListaEstudiantes.Insert(pos, nuevo);
			}
			else
			{
				Console.WriteLine("la posicion ingresada no esta dentro del rango.");
			}
		}

		// METODO ELIMINAR EL PRIMERO
		public void eliminarElPrimero()
		{
			if (ListaEstudiantes.Count > 0)
			{
				ListaEstudiantes.RemoveAt(0);
			}
			else
			{
				Console.WriteLine("No hay elementos para eliminar.");
			}
		}

		// METODO ELIMINAR EL ULTIMO
		public void eliminarElUltimo()
		{
			if (ListaEstudiantes.Count > 0)
			{
				int tamanio = ListaEstudiantes.Count;
				ListaEstudiantes.RemoveAt(tamanio - 1);
			}
			else
			{
				Console.WriteLine("No hay elementos para eliminar.");
			}
		}

		// METODO ELIMINAR EL DE LA POSICION DADA
		public void eliminarElDeLaPosicion(int pos)
		{
			if (ListaEstudiantes.Count > 0 && pos >= 0 && pos < ListaEstudiantes.Count)
			{
				ListaEstudiantes.RemoveAt(pos);
			}
			else
			{
				Console.WriteLine("la posicion ingresada no esta dentro del rango.");
			}
		}

		// METODO LISTAR ELEMENTOS
		public void listar()
		{
			Estudiante[] estudiantes = ListaEstudiantes.ToArray();
			for (int i = 0; i < estudiantes.Length; i++)
			{
				Console.WriteLine(estudiantes[i] + " en posicion: " + i);
			}
		}

		// METODO DAR FECHA
		public String darFecha()
        {
			return DateTime.Now.ToString("dd-mm-yyyy");
        }

	}
}

﻿using ListasSimples.mundo;
using System;

namespace ListasSimples
{
    class Program
    {
        static void Main(string[] args)
        {
			Lista miLista = new Lista();

			String continuar = "s";
			while (continuar.Equals("s") || continuar.Equals("S"))
			{

				Console.WriteLine("Ingrese el numero de una opcion:"
						+ "\n1. Insertar un libro al principio de la lista"
						+ "\n2. Insertar un libro al final de la lista "
						+ "\n3. Insertar un libro despues de una posicion dada "
						+ "\n4. Obtener el libro de una posicion dada de la lista "
						+ "\n5. Dar tamanio de la lista "
						+ "\n6. Preguntar si la lista esta vacia "
						+ "\n7. Eliminar el primer libro de la lista "
						+ "\n8. Eliminar el ultimo libro de la lista "
						+ "\n9. Eliminar un libro dada la posicion en la lista "
						+ "\n10. Ver los libros exisentes en la lista "
						+ "\n11. Calcular el promedio de precios de los libros. ");

				int opcion = int.Parse(Console.ReadLine());

				switch (opcion)
				{
					case 1:
						{
							Console.WriteLine("Ingrese el titulo del libro: ");					
							String titulo = Console.ReadLine();

							Console.WriteLine("Ingrese el autor del libro: ");
							String autor = Console.ReadLine();

							Console.WriteLine("Ingrese el ISBN del libro: ");
							String isbn = Console.ReadLine();

							Console.WriteLine("Ingrese el precio del libro: ");
							double precio = double.Parse(Console.ReadLine());

							Libro libro = new Libro(titulo, autor, isbn, precio);
							miLista.insertarAlPrincipio(libro);
							break;
						}

					case 2:
						{
							Console.WriteLine("Ingrese el titulo del libro: ");
							String titulo = Console.ReadLine();

							Console.WriteLine("Ingrese el autor del libro: ");
							String autor = Console.ReadLine();

							Console.WriteLine("Ingrese el ISBN del libro: ");
							String isbn = Console.ReadLine();

							Console.WriteLine("Ingrese el precio del libro: ");
							double precio = double.Parse(Console.ReadLine());

							Libro libro = new Libro(titulo, autor, isbn, precio);
							miLista.insertarAlFinal(libro);
							break;
						}

					case 3:
						{
							Console.WriteLine("Ingrese el titulo del libro: ");
							String titulo = Console.ReadLine();

							Console.WriteLine("Ingrese el autor del libro: ");
							String autor = Console.ReadLine();

							Console.WriteLine("Ingrese el ISBN del libro: ");
							String isbn = Console.ReadLine();

							Console.WriteLine("Ingrese el precio del libro: ");
							double precio = double.Parse(Console.ReadLine());

							Console.WriteLine("Ingrese la posicion: ");
							int posicion = int.Parse(Console.ReadLine());

							Libro libro = new Libro(titulo, autor, isbn, precio);
							miLista.insertarDespues(posicion, libro);
							break;
						}

					case 4:
						{
							Console.WriteLine("Ingrese la posicion: ");
							int posicion = int.Parse(Console.ReadLine());
							Console.WriteLine(miLista.obtener(posicion));
							break;
						}

					case 5:
						{
							Console.WriteLine("El tamanio de la lista es: " + miLista.darTamanio());
							break;
						}

					case 6:
						{
							Console.WriteLine("¿La lista esta vacia? R// " + miLista.estaVacia());
							break;
						}

					case 7:
						{
							miLista.eliminarElprimero();
							break;
						}

					case 8:
						{
							miLista.eliminarElUltimo();
							break;
						}

					case 9:
						{
							Console.WriteLine("Ingrese la posicion: ");
							int posicion = int.Parse(Console.ReadLine());
							miLista.eliminarEnPosicion(posicion);
							break;
						}

					case 10:
						{
							miLista.verLibros();
							break;
						}

					case 11:
						{
							Console.WriteLine("El promedio de precios de los libros es: " + miLista.calcularPromedioPrecioLibros());
							break;
						}

					default:
						{
							Console.WriteLine("Opcion invalida.");
							break;
						}
				}

				Console.WriteLine("Escriba S si desea continuar. Otra letra en caso contrario. ");
				continuar = Console.ReadLine();
				Console.WriteLine();
			}
		}
    }
}

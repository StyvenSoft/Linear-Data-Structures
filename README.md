# Estructuras de Datos Lineales

## Arreglos

- **Ver Arreglos**
    
    [Arreglos](https://www.notion.so/Arreglos-3e4d4988392647899c697db792c09638)
    

## Listas enlazadas

- **Ver Listas enlazadas**
    
    [Listas](https://www.notion.so/Listas-4c964f456a6d423a8dbdbf6d0f1f6caf)
    

## Pilas

- **Ver Pilas**
    
    [Pilas - Stack<T>](https://www.notion.so/Pilas-Stack-T-b19f00b52ad6453e965e79afaa29df75)
    

## Colas

- **Ver Colas**
    
    [Colas - Queue<T>](https://www.notion.so/Colas-Queue-T-52f79d0b78734c7994a8a2e9e552c059)
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fase3ColaConsola.mundo
{
    class Tiquete
    {
		// ATRIBUTOS
		private int numero;
		private String idCliente;
		private String nombreCliente;
		private String aerolinea;
		private String destino;
		private String fecha;

		// METODO CONTRUCTOR
		public Tiquete(int numero, String idCliente, String nombreCliente,
				String aerolinea, String destino, String fecha)
		{
			this.numero = numero;
			this.idCliente = idCliente;
			this.nombreCliente = nombreCliente;
			this.aerolinea = aerolinea;
			this.destino = destino;
			this.fecha = fecha;
		}

		// METODOS GET Y SET
		public int getNumero()
		{
			return numero;
		}
		public void setNumero(int numero)
		{
			this.numero = numero;
		}
		public String getIdCliente()
		{
			return idCliente;
		}
		public void setIdCliente(String idCliente)
		{
			this.idCliente = idCliente;
		}
		public String getNombreCliente()
		{
			return nombreCliente;
		}
		public void setNombreCliente(String nombreCliente)
		{
			this.nombreCliente = nombreCliente;
		}
		public String getAerolinea()
		{
			return aerolinea;
		}
		public void setAerolinea(String aerolinea)
		{
			this.aerolinea = aerolinea;
		}
		public String getDestino()
		{
			return destino;
		}
		public void setDestino(String destino)
		{
			this.destino = destino;
		}
		public String getFecha()
		{
			return fecha;
		}
		public void setFecha(String fecha)
		{
			this.fecha = fecha;
		}

		// METODO TOSTRING
		override
		public String ToString()
		{
			return "Tiquete [numero=" + numero + ", idCliente=" + idCliente + ", nombreCliente=" + nombreCliente
					+ ", aerolinea=" + aerolinea + ", destino=" + destino + ", fecha=" + fecha + "]";
		}
	}
}

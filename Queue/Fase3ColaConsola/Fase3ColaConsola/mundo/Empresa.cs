﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fase3ColaConsola.mundo
{
    class Empresa
    {
		// ATRIBUTOS
		private Queue<Tiquete> colaAerocivil;

		// CONTRUCTOR
		public Empresa()
		{
			colaAerocivil = new Queue<Tiquete>();
		}

		// AGREGAR - ENCOLAR
		public void agregarTiquete(int numero, String idCliente, String nombreCliente,
				String aerolinea, String destino, String fecha)
		{
			Tiquete nuevo = new Tiquete(numero, idCliente, nombreCliente, aerolinea,
					destino, fecha);
			colaAerocivil.Enqueue(nuevo); // encolar
		}

		// ELIMINAR - DESENCOLAR
		public void eliminarTiquete()
		{
			colaAerocivil.Dequeue(); // desencolar
		}

		// PROXIMO A PROCESAR
		public Tiquete obtenerProximoAProcesar()
		{
			return colaAerocivil.Peek();
		}

		// TAMANIO DE LA COLA
		public int cantidadEnCola()
		{
			return colaAerocivil.Count;
		}

		// LISTAR TODOS LOS ELEMENTOS DE LA COLA
		public void listar()
		{
			Tiquete [] tiquetes = colaAerocivil.ToArray();
			for (int i = 0; i < tiquetes.Length; i++)
			{
				Console.WriteLine(tiquetes[i] + " posicion: " + i);
			}
		}
	}
}

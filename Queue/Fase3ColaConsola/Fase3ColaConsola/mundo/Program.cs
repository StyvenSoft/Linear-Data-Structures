﻿using Fase3ColaConsola.mundo;
using System;

namespace Fase3ColaConsola
{
    class Program
    {
        static void Main(string[] args)
        {
			Empresa ac = new Empresa();
			ac.agregarTiquete(1, "111", "Celso", "Avianca", "Bogota", "22-12-2021");
			ac.agregarTiquete(2, "222", "Javier", "EasyFly", "Medellin", "4-01-2022");
			ac.agregarTiquete(3, "333", "Gafis", "LATAM", "Cartagena", "15-06-2022");
			ac.listar();
			Console.WriteLine("\nEliminando el primero que entro...");
			ac.eliminarTiquete();
			ac.listar();
			Console.WriteLine("\nEl que esta de primero y sera el proximo a eliminar...");
			Console.WriteLine(ac.obtenerProximoAProcesar());
			Console.WriteLine("\nCuantos hay en la cola...");
			Console.WriteLine(ac.cantidadEnCola());
		}
    }
}

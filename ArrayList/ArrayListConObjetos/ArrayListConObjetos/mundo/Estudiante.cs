﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArrayListConObjetos.mundo
{
    public class Estudiante
    {
        // ATRIBUTOS
        public String codigo { get; set; }
        public String nombre { get; set; }
        public int semestre { get; set; }
        public String curso { get; set; }

        // METODO CONSTRUCTOR
        public Estudiante(String codigo, String nombre, int semestre, String curso)
        {
            this.codigo = codigo;
            this.nombre = nombre;
            this.semestre = semestre;
            this.curso = curso;
        }
        override
        public String ToString()
        {
            return "Estudiante - Codigo: " + codigo + " - Nombre: " + nombre +
                " - Semestre: " + semestre + " - Curso: " + curso;
        }
    }
}

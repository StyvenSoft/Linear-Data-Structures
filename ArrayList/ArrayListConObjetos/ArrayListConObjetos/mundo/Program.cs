﻿using ArrayListConObjetos.mundo;
using System;
using System.Collections;

namespace ArrayListConObjetos
{
    public class Program
    {
		// ATRIBUTOS
		private ArrayList estudiantes;

		// METODO CONSTRUCTOR
		public Program()
		{
			estudiantes = new ArrayList();
			estudiantes.Add(new Estudiante("111", "Celso", 3, "POO 2"));
			estudiantes.Add(new Estudiante("222", "Javier", 5, "Estructuras de Datos"));
			estudiantes.Add(new Estudiante("333", "William", 7, "Computación gráfica"));
			estudiantes.Add(new Estudiante("444", "Carlos", 2, "POO 1"));
			estudiantes.Add(new Estudiante("555", "Ivan", 1, "Introducción a la Ingenieria"));
		}

		public Estudiante buscarEstudiante(String codigo)
		{
			Estudiante buscado = null;
			bool centinela = false;
			for (int i = 0; i < estudiantes.Count && centinela == false; i++)
			{
				Estudiante temp = (Estudiante) estudiantes[i];
				if (temp.codigo.Equals(codigo))
				{
					buscado = temp;
					centinela = true;
				}
			}
			return buscado;
		}

		public static void Main(String[] args)
		{
			Program miPrograma = new Program();
			Console.WriteLine("Digite el codigo del estudiante a buscar: ");
			String codigo = Console.ReadLine();
			Console.WriteLine("El estudiante buscado es: " + miPrograma.buscarEstudiante(codigo));

		}
	}
}
